#!/bin/bash

SERVICE_NAME="homebridge"
SYSTEMD_LINK_DIR="/etc/systemd/system"

error_exit() {
    echo "Error: $*"
    exit 1
}

if [[ $UID -ne 0 ]]; then
    error_exit "Need to be root, please run with elevated permissions. (hint: sudo)"
fi

adduser --system --home /home/homebridge --shell /bin/false --disabled-password --disabled-login --group homebridge

TARGET_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/$SERVICE_NAME.service"
LINK_PATH="$SYSTEMD_LINK_DIR/$SERVICE_NAME.service"

if [[ -f "$LINK_PATH" ]]; then
    if cmp -s "$TARGET_PATH" "$LINK_PATH"; then
        error_exit "Service $SERVICE_NAME is already installed."
    fi
    error_exit "Some other service named $SERVICE_NAME is already installed at $LINK_PATH"
fi


ln -s "$TARGET_PATH" "$LINK_PATH" || error_exit "Error installing systemd service $SERVICE_NAME"
echo "Installation successful."
