# Homebridge Systemd Unit

This is a systemd unit for running [homebridge](https://github.com/nfarina/homebridge).

## Installation

With elevated privileges, please run

```
./install.sh
```

This will create a system user named homebridge, and symlink the provided `.service` file to `/etc/systemd/system/`, after which you may run

```
systemctl enable homebridge
```

To enable the service at boot.

### Additional configuration

Please read the provided information from the main project repository ([homebridge](https://github.com/nfarina/homebridge)).
